# Documentação do Projeto da Lâmpada RGB de ultra precisão

## 1. Introdução

O projeto da "Lâmpada RGB de ultra precisão" foi realizado para a diciplina de Introdução a Engenharia do primeiro período do curso de Engenharia da Computação na UTFPR.


## 2. Objetivos
* Criar uma lâmpada que é possível o usuário informar o código RGB da cor da lâmpada.
* Regular o contraste da cor selecionada.
* Entrada de dados a partir de um teclado matricial.

## 3. Materiais utilizados

### Lista de Materiais
 * 1 Arduíno UNO R3
 * 1 Potenciômetro
 * 3 Leds nas cores vermelha, verde e azul
 * 1 Botão
 * 6 Leds RGBs 
 * 3 Resistores de 220 ohms
 * 1 Teclado Matricial 4 x 3
  Observação: o teclado utilizado no simulador é do tipo 4 x 4, porém é dispensável a utilização da ultima coluna, o que permite o uso do teclado 4 x 3.

## 4. Esquema Elétrico

O esquema elétrico pode ser visto por:

![](Projeto.png)

## 5. Código

```Cpp
int red, green, blue;

int teclado ( void );

void setup()
{
  // led normal
  pinMode(A5, OUTPUT);
  pinMode(A4, OUTPUT);
  pinMode(A3, OUTPUT);
  
  // led rgb
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  
  pinMode(A2, OUTPUT);
  
  // pinos linhas - 0 1 2 3 
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  
   // pinos colunas - 4 5 6
  pinMode(6, INPUT_PULLUP);
  pinMode(7, INPUT_PULLUP);
  pinMode(8, INPUT_PULLUP);
  
  pinMode(13, INPUT_PULLUP);
  
  Serial.begin(9600);
}

void loop()
{
  red = green = blue = 0; 
  
  analogWrite(A2, 255);
  
  analogWrite(A5, 255);
  red = teclado(  );
  analogWrite(A5, 0);
 
  analogWrite(A4, 255);
  green = teclado (  );
  analogWrite(A4, 0);
    
  analogWrite(A3, 255);
  blue = teclado (  ); 
  analogWrite(A3, 0);
  
  analogWrite(9, red);
  analogWrite(10, blue);
  analogWrite(11, green);

  //-------------------------------------------
  
  while ( digitalRead(13) == LOW ){}
  
  analogWrite(9, 0);
  analogWrite(10, 0);
  analogWrite(11, 0);
  
}

int teclado ( void )
{
  
  int matriz[4][3] = {{1,2,3},{4,5,6},{7,8,9},{-1,0,-1}}, num = 0, aux = 0;
  
  for ( int i = 2; 1 == 1; i++ )
  {
  	digitalWrite( 2, HIGH );
    digitalWrite( 3, HIGH );
    digitalWrite( 4, HIGH );
    digitalWrite( 5, HIGH );
    digitalWrite( i, LOW );
    
    if ( digitalRead(6) == LOW )
    {
    	num = matriz[i - 2][0];
        aux = (aux * 10) + num;
        while ( digitalRead(6) == LOW ){}
    }
    
    if ( digitalRead(7) == LOW )
    {
    	num = matriz[i - 2][1];
      	aux = (aux * 10) + num;
        while ( digitalRead(7) == LOW ){}
    }
    if ( digitalRead(8) == LOW )
    {
     	if ( i == 5 )
    	{
    		break;
    	}
      
    	num = matriz[i - 2][2];
        aux = (aux * 10) + num;
        while ( digitalRead(8) == LOW ){}
    }
    
    if ( i == 5 )
    {
    	i = 1;
    }
  	
    delay(10);
  }
  
  delay(1000);
  return aux;
  
}
```

## 6. Resultados

O vídeo de demonstração pode ser visto em:
{{< youtube RQ13_h7nR28 >}}


Vídeo realizado por: Thassiana Amorim

## 7. Desafios encontrados

* Programação do teclado matricial.
* Distância dos integrantes do grupo.
* Na utilização do simulador que apresentava delay incompatível com o esperado. 
