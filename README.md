# Introdução

- Projeto realizado por alunos da UTFPR - Universidade Tecnologica Federal do Paraná

- Lâmpada RGB e ultra precisão:
    
Esse sistema possibilita que o usuário escolha a cor da lâmpada apartir de valores da tabela de cores RGB, além de regular o seu contraste.

![](Projeto.png)


## Equipe


O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
| Isaac Fiuza Vieira | [@isaacfvi1](https://gitlab.com/isaacfvi1)|
| Thassiana Camilia Amorim Muller | [@thassi](https://gitlab.com/thassi) |
| Angelo Amaro Dos Santos Furtado Júnior | [@Angelo_](https://gitlab.com/Angelo_) |

# Documentação

A documentação do projeto pode ser acessada pelo link [aqui](https://isaacfvi1.gitlab.io/ie21cp20201/).

# Links Úteis

* [Tinkercad](https://www.tinkercad.com/things/2XOxnSDtw8f)
* [Documentação](https://isaacfvi1.gitlab.io/ie21cp20201/)
