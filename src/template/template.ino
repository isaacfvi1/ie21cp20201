int red, green, blue;

int teclado ( void );

void setup()
{
  // led normal
  pinMode(A5, OUTPUT);
  pinMode(A4, OUTPUT);
  pinMode(A3, OUTPUT);
  
  // led rgb
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  
  pinMode(A2, OUTPUT);
  
  // pinos linhas - 0 1 2 3 
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  
   // pinos colunas - 4 5 6
  pinMode(6, INPUT_PULLUP);
  pinMode(7, INPUT_PULLUP);
  pinMode(8, INPUT_PULLUP);
  
  pinMode(13, INPUT_PULLUP);
  
  Serial.begin(9600);
}

void loop()
{
  red = green = blue = 0; 
  
  analogWrite(A2, 255);
  
  analogWrite(A5, 255);
  red = teclado(  );
  analogWrite(A5, 0);
 
  analogWrite(A4, 255);
  green = teclado (  );
  analogWrite(A4, 0);
    
  analogWrite(A3, 255);
  blue = teclado (  ); 
  analogWrite(A3, 0);

  Serial.print("red: ");
  Serial.println(red);
  Serial.print("Green: ");
  Serial.println(green);
  Serial.print("Blue: ");
  Serial.println(blue);
  
  analogWrite(9, red);
  analogWrite(10, blue);
  analogWrite(11, green);

  //-------------------------------------------
  
  while ( digitalRead(13) == LOW ){}
  
  analogWrite(9, 0);
  analogWrite(10, 0);
  analogWrite(11, 0);
  
}

int teclado ( void )
{
  
  int matriz[4][3] = {{1,2,3},{4,5,6},{7,8,9},{-1,0,-1}}, num = 0, aux = 0;
  
  for ( int i = 2; 1 == 1; i++ )
  {
  	digitalWrite( 2, HIGH );
    digitalWrite( 3, HIGH );
    digitalWrite( 4, HIGH );
    digitalWrite( 5, HIGH );
    digitalWrite( i, LOW );
    
    if ( digitalRead(6) == LOW )
    {
    	num = matriz[i - 2][0];
        aux = (aux * 10) + num;
        while ( digitalRead(6) == LOW ){}
    }
    
    if ( digitalRead(7) == LOW )
    {
    	num = matriz[i - 2][1];
      	aux = (aux * 10) + num;
        while ( digitalRead(7) == LOW ){}
    }
    if ( digitalRead(8) == LOW )
    {
     	if ( i == 5 )
    	{
    		break;
    	}
      
    	num = matriz[i - 2][2];
        aux = (aux * 10) + num;
        while ( digitalRead(8) == LOW ){}
    }
    
    if ( i == 5 )
    {
    	i = 1;
    }
  	
    delay(10);
  }
  
  delay(1000);
  return aux;
  
}


